let inputNumber, multipleNumber;
const multiple = 5;


do {
  inputNumber = prompt('Input number');
} while (
  !inputNumber ||
  typeof +inputNumber !== 'number' ||
  isNaN(+inputNumber) ||
  inputNumber.trim() === "" ||
  Math.round(+inputNumber) !== +inputNumber
);

inputNumber = +inputNumber;

if (Math.abs(inputNumber) >= multiple) {
  if (inputNumber >= 0) {
    for (i = 1; i <= inputNumber; i++) {
      if (!(i % multiple)) {
        console.log(i);
      }
    }
  } else {
    for (i = -1; i >= inputNumber; i--) {
      if (!(i % multiple)) {
        console.log(i);
      }
    }
  };
} else {
  console.log('Sorry, no numbers');
}