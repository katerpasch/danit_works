'use strict';

let m, n;

  m = +prompt('Input number m');
  n = +prompt('Input number n');

if ( !n || !m || isNaN(m) || isNaN(n) || !(m < n) || ( m < 2) || ( n < 2)) { 
  alert('Error, input again');
} else {
  nextPrime:
  for (let i = m; i <= n; i++){
      for (let j = 2; j < i; j++){
       if (i % j === 0) continue nextPrime;
      }
      console.log(i);
    };
};

