let userName, userAge;

do {
  userName = prompt('What is your name', userName);
  userAge = prompt('How old are you', userAge);
  console.log(typeof userName);
} while (
  !userName ||
  typeof userName !== 'string' ||
  userName.trim() === "" ||
  
  !userAge ||
  typeof +userAge !== 'number' ||
  isNaN (+userAge) ||
  userAge.trim() === ""
);

if (userAge < 18) {
  alert('You are not allowed to visit this website.');
} 
else if ((userAge >= 18) && (userAge <= 22)) {
  let answer = confirm('Are you sure you want to continue?');
  let message = (answer === false) ? 'You are not allowed to visit this website.' : ('Welcome, ' + userName);
  alert(message);
} 
else {
  alert(`Welcome, ${userName}`);  
}
